package vn.fahu.keycloak.resetpassword.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.UriBuilder;

import org.keycloak.authentication.actiontoken.resetcred.ResetCredentialsActionToken;
import org.keycloak.common.util.Time;
import org.keycloak.models.Constants;
import org.keycloak.models.KeycloakContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.services.resources.LoginActionsService;

public class ResetPasswordTokenResource {
	
	private KeycloakSession session;
	
	public ResetPasswordTokenResource(KeycloakSession session) {
		this.session = session;
	}

	@GET
	@Path("url")
	public Object getPasswordResettingUrl(
			final @QueryParam("username") String username,
			final @QueryParam("client") String clientId
	) {
		KeycloakContext context = this.session.getContext();
		RealmModel realm = context.getRealm();
		UserModel user = this.session.users().getUserByUsername(username, realm);
		int validityInSecs = context.getRealm().getActionTokenGeneratedByUserLifespan(ResetCredentialsActionToken.TOKEN_TYPE);
		int absoluteExpirationInSecs = Time.currentTime() + validityInSecs;
		
		ResetCredentialsActionToken token = new ResetCredentialsActionToken(
				user.getId(),
				absoluteExpirationInSecs,
				null,
				clientId
			);
		
		UriBuilder builder = LoginActionsService.actionTokenProcessor(session.getContext().getUri());
		builder.queryParam(Constants.KEY, token.serialize(session, realm, session.getContext().getUri()));

		return builder.build(realm.getName()).toString();
	}
}
