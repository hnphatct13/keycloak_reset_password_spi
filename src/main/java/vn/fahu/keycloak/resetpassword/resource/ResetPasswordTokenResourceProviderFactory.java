package vn.fahu.keycloak.resetpassword.resource;

import org.keycloak.Config.Scope;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.services.resource.RealmResourceProvider;
import org.keycloak.services.resource.RealmResourceProviderFactory;

public class ResetPasswordTokenResourceProviderFactory implements RealmResourceProviderFactory {
	
	private static final String RESET_PASSWORD_TOKEN_API_SPI_ID = "resetpassword-token-api";

	@Override
	public RealmResourceProvider create(KeycloakSession session) {
		return new ResetPasswordTokenResourceProvider(session);
	}

	@Override
	public void init(Scope config) {
		
	}

	@Override
	public void postInit(KeycloakSessionFactory factory) {
		
	}

	@Override
	public void close() {
		
	}

	@Override
	public String getId() {
		return RESET_PASSWORD_TOKEN_API_SPI_ID;
	}

}
