package vn.fahu.keycloak.resetpassword.resource;

import org.keycloak.models.KeycloakSession;
import org.keycloak.services.resource.RealmResourceProvider;

public class ResetPasswordTokenResourceProvider implements RealmResourceProvider {

	private KeycloakSession session;
	
	public ResetPasswordTokenResourceProvider(KeycloakSession session) {
		this.session = session;
	}
	
	@Override
	public void close() {
	}

	@Override
	public Object getResource() {
		return new ResetPasswordTokenResource(session);
	}

}
